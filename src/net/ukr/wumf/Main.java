package net.ukr.wumf;

public class Main {

	public static void main(String[] args) {

		int mas[] = new int[200000000];

		for (int i = 0; i < mas.length; i++) {
			mas[i] = (int) (Math.random() * 9);

		}

		Sum thOne = new Sum(mas, 0, 50000000);

		Sum thTwo = new Sum(mas, 50000000, 100000000);

		Sum thThree = new Sum(mas, 100000000, 150000000);

		Sum thFour = new Sum(mas, 150000000, 199999999);

		Thread one = new Thread(thOne);

		Thread two = new Thread(thTwo);

		Thread three = new Thread(thThree);

		Thread four = new Thread(thFour);

		one.start();

		two.start();

		three.start();

		four.start();

		try {
			one.join();

			two.join();

			three.join();

			four.join();

		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		long t = thOne.getSum() + thTwo.getSum() + thThree.getSum() + thFour.getSum();

		System.out.println("   = "+ t);
		
	}
}
