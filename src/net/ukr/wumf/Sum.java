package net.ukr.wumf;

public class Sum implements Runnable {

	private long sum;

	private int[] mas;

	private int start;

	private int end;

	public Sum(int[] mas, int start, int end) {
		super();
		this.mas = mas;
		this.start = start;
		this.end = end;
	}

	public long getSum() {
		return sum;
	}

	public int[] getMas() {
		return mas;
	}

	public void setMas(int[] mas) {
		this.mas = mas;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	private long summa(int[] mass) {

		for (int i = start; i <= end; i++) {
			sum += mass[i];

		}
		return sum;

	}

	@Override
	public void run() {
        Thread th = Thread.currentThread();

		System.out.println(" ����� ������ id " + th.getId() + " = "  + summa(mas));

	}
}
